<?php

namespace Tests\Feature\Http\Controllers\Api\v1;

use Tests\TestCase;

use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CategoryControllerTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @test
     */
    public function can_create__a_category()
    {

      $faker  = \Faker\Factory::create();
      $response = $this->json('POST','/api/v1/categories',[
          'name' => $faker->company,
          'marker'=> $faker->sentences
      ]);

      $response->assertStatus(200);
    }
}
