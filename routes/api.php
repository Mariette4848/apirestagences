<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('login', 'API\v1\User\UserController@login');


Route::middleware('jwt.auth')->get('users', function () {
    return auth('api')->user();
});


Route::resource('v1/categories','API\v1\Category\CategoryController');
Route::resource('v1/agences','API\v1\Agence\AgenceController');
